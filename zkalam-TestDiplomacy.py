# Diplomacy Unit Tests
# 
# Authors:
#   Zuhayer Kalam 
#   Yue Taira 

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve

# TestDiplomacy
class TestDiplomacy (TestCase):

    # test read
    
    def test_read1(self):
        s = 'A Austin Hold'
        [command, Warzone, Conflicts] = diplomacy_read(s)
        self.assertEqual(command[0][0], 'A')
        self.assertEqual(command[0][1], 'Austin')
        self.assertEqual(command[0][2], 'Hold')
        
    def test_read2(self):
        s = 'A Austin Move Houston'
        [command, Warzone, Conflicts] = diplomacy_read(s)
        self.assertEqual(command[0][0], 'A')
        self.assertEqual(command[0][1], 'Austin')
        self.assertEqual(command[0][2], 'Move')
        self.assertEqual(command[0][3], 'Houston')
        
    def test_read3(self):
        s = 'A Austin Hold\nB Dallas Move Texarkana'
        [command, Warzone, Conflicts] = diplomacy_read(s)
        self.assertEqual(command[0], ['A', 'Austin', 'Hold'])
        self.assertEqual(command[1], ['B','Dallas','Move', 'Texarkana'])
        
    def test_read4(self):     
        with self.assertRaises(AssertionError):
            diplomacy_read('A Austin Hello')
            
    
    def test_read5(self):   
        with self.assertRaises(AssertionError):
            diplomacy_read('A Austin')
    
    def test_read6(self):   
        with self.assertRaises(AssertionError):
            diplomacy_read('A Austin Move')
            
    def test_read7(self):   
        with self.assertRaises(AssertionError):
            diplomacy_read('a Austin Hold')

    def test_read8(self):   
        with self.assertRaises(AssertionError):
            diplomacy_read('A austin Hold')
            
    def test_read9(self):   
        with self.assertRaises(AssertionError):
            diplomacy_read('A Austin Hold\nA Dallas Hold')

    def test_read10(self):   
        with self.assertRaises(AssertionError):
            diplomacy_read('A Austin Hold\nB Austin Hold')

    # test solve
    
    def test_solve1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
    
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
            
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London\n")
    
    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support I\nF Dallas Move Houston\nG SanAntonio Support F\nH Waco Move Houston\nI Houston Move Texarkana\nJ ElPaso Support I")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Austin\nF [dead]\nG SanAntonio\nH [dead]\nI Texarkana\nJ ElPaso\n")
    
    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Austin Move London\nE Dallas Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\n")

    def test_solve6(self):
        r = StringIO("A Madrid Move Dallas\nB Barcelona Move Madrid\nC Austin Move Madrid\nD Waco Support D\nE Houston Move Waco\nF Temple Move Waco")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Dallas\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")
    
    def test_solve7(self):
        w = StringIO()
        with self.assertRaises(AssertionError):
            diplomacy_solve('A Austin Hold\nB Dallas Support Austin',w)



# __main__

if __name__ == "__main__":
    main()

